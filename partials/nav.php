<!-- =====================================
==== Start Navbar -->

<nav class="navbar navbar-expand-lg">
    <div class="container">

    <!-- Logo -->
    <a class="logo" href="#">
        <img src="img/logoblanco.png" alt="logo">          
    </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"><i class="fas fa-bars"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link active" href="#" data-scroll-nav="0">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-scroll-nav="2">Servicios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://webdesign.wemanagement.com.ar/">Programación Web</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="1">Nosotros</a>
            </li>
            <!-- <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="3">Portfolio</a>
            </li> -->
            <!-- <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="5">Blog</a>
            </li> -->
            <li class="nav-item">
            <a class="nav-link" href="#" data-scroll-nav="6">Contacto</a>
            </li>
        </ul>
        </div>
    </div>
</nav>

<!-- End Navbar ====
======================================= -->
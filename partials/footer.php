 <!-- =====================================
==== Start Footer -->

<footer class="text-center">
    <div class="container">

        <!-- Logo -->
        <a class="logo" href="#">
            <img src="img/logoblanco.png" alt="logo">          
        </a>
        
        <div class="social">
            <a href="https://www.facebook.com/wemanagement.ok/" class="icon">
                <i class="fab fa-facebook-f"></i>
            </a>
            <!-- <a href="#0" class="icon">
                <i class="fab fa-twitter"></i>
            </a> -->
            <!-- <a href="#0" class="icon">
                <i class="fab fa-linkedin-in"></i>
            </a> -->
            <a href="https://www.instagram.com/wemanagement.ok/" class="icon">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/company/wemanagement" class="icon">
                <i class="fab fa-linkedin"></i>
            </a>
        </div>

        <p>&copy; 2020 | We Management</p>

    </div>
</footer>

<!-- End Footer ====
======================================= -->

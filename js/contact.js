var form = document.querySelector('#contact-form');

form.addEventListener('submit', function(event) {
    event.preventDefault();
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">Completa este campo.</span>';
        return false;
    }else{
        document.getElementById('g-recaptcha-error').innerHTML = '';
        Swal.fire(
            '¡Enviado correctamente!',
            '¡Gracias por escribirnos, nos pondremos en contacto a brevedad!',
            'success'
            )
            let formData = new FormData(form);
            form.reset();
            fetch("contact.php",
            {
                body: formData,
                method: "post"
            }
        );
    }
});

var form2 = document.querySelector('#suscribe');

form2.addEventListener('submit', function(event) {
    event.preventDefault();
    Swal.fire(
        '¡Enviado correctamente!',
        '¡Muchas gracias por suscribirte!',
        'success'
        )
        let form2Data = new FormData(form2);
        form2.reset();
        fetch("contact.php",
        {
            body: form2Data,
            method: "post"
        }
    );
});
<!DOCTYPE html>
 <html lang="es">

    <?php include ('partials/head.php'); ?>

    <body>

     <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFVDGL6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    	<!-- =====================================
    	==== Start Loading -->

    	<div class="loading">
    		<div class="text-center middle">
    			<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    		</div>
    	</div>
        
    	<!-- End Loading ====
    	======================================= -->

        <?php include ('partials/nav.php'); ?>

        <?php include ('partials/header.php'); ?>


        <!-- =====================================
        ==== Start Services -->

        <section class="services section-padding" data-scroll-index="2">
            <div class="container">
                <div class="row">

                    <div class="section-head text-center col-sm-12">
                        <h4>Qué hacemos?</h4>
                        <h6>We Management</h6>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-book-pencil"></span>
                            <h6>Diseño Web</h6>
                            <p>Hacemos páginas web, ya sea informativa o un sistema a medida. Optimizadas para todos los dispositivos</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-pencil-ruler"></span>
                            <h6>Campañas Publicitarias</h6>
                            <p>Posicionamos tu Marca en Internet, hacemos campañas de publicidad en Redes Sociales y Google.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-anchor"></span>
                            <h6>Community Management</h6>
                            <p>Somos responsables de la gestión y el desarrollo de la comunidad online de tu marca o empresa en el mundo digital, manteniendo relaciones estables y dudaderas con sus clientes.</p>
                        </div>
                    </div>

                    <hr>

                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-picture"></span>
                            <h6>Diseño Creativo</h6>
                            <p>Diseñamos contenido digital para tu empresa, ya sean imágenes para subir en las redes, diseños web, manual de marca, imágenes para anuncios, etc...</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-sm50">
                            <span class="icon icon-basic-ipod"></span>
                            <h6>E-Commerce</h6>
                            <p>Tiendas online con un diseño profesional, a medida y optimizado para todos los dispositivos. Te dejamos la web lista para que la puedas gestionar en un back-office amigable. Trabajamos con Mercado Shops, Tienda Nube, Shopify o podemos armarte una tienda a medida.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="item">
                            <span class="icon icon-basic-gear"></span>
                            <h6>Soporte Personalizado</h6>
                            <p>Vas a poder contactarte con cualquiera de nuestro equipo para que te responda las preguntas que tengas, en cualquier momento. Te enviamos un Informe Mensual con todas las métricas de tu empresa.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Services ====
        ======================================= -->


        <!-- =====================================
        ==== Start Section-box -->

        <section class="section-box" data-scroll-index="1">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-lg-6 bg-img half-img bgimg-height" data-background="img/business-computer-device-35550.jpg"></div>

                    <div class="col-lg-6 half-content bg-gray">
                        <div class="box-white">

                            <div class="content mb-50">
                                <h5 class="sm-line-height">Marketing is not about the things you do, is about the stories you tell.</h5>
                                <p class="mb-15">Seth Godin</p>
                                <p>Hace años que ayudamos a Pymes a generar nuevas oportunidades de negocio utilizando las redes como nuestro gran pilar, posicionándolo en la web con estretategias y contenido en el universo online, con el objetivo de alcanzar potenciales clientes  mediante un diálogo transparente, ganando credibilidad y confianza, asi como lograr un feedback totalmente personalizado.</p>
                            </div>

                            <!-- Skills -->

                            <div class="skills">
                                <div class="skill-item">
                                    <h6>Diseño Creativo</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="90%"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <h6>Community Management</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="85%"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <h6>Campañas Publicitarias</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="99%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Section-box ====
        ======================================= -->


        <!-- =====================================
        ==== Start Hero -->

        <section class="hero section-padding" data-scroll-index="1">
            <div class="container">
                <div class="row">
                    
                    <div class="intro offset-lg-1 col-lg-10 text-center mb-80">
                        <!-- <h3>BIENVENIDO/A</h3> -->
                        <h4>Quiénes Somos?</h4>
                        <p>Somos un equipo de profesionales creativos que comprende cómo desarrollar y utilizar el contenido como un medio para aumentar tus ingresos y construir relaciones significativas. Creamos contenido de valor y relevante orientado a generar engagement, loyalty y conversión. Desarrollamos e implementamos un plan táctico de Content Marketing para que la marca pueda construir lazos fuertes a largo plazo con los consumidores de mayor valor.</p>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-book-pen"></span>
                            <h5>Diseño Creativo</h5>
                            <p>Diseñamos contenido digital para tu empresa, ya sean imágenes para subir en las redes, diseños web, manual de marca, imágenes para anuncios, etc...</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-cards-diamonds"></span>
                            <h5>Community Management</h5>
                            <p>El community management es sin lugar a dudas la mejor manera de encontrar buenos clientes. Somos responsables de la gestión y el desarrollo de la comunidad online de su marca o empresa en el mundo digital, manteniendo relaciones estables y duraderas con sus clientes.</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center">
                            <span class="icon icon-basic-pencil-ruler"></span>
                            <h5>Campañas Publicitarias</h5>
                            <p>Posicionamos tu Marca en Internet, hacemos campañas publicitarias en Google y Redes Sociales. Todo tipo de campañas ya sea para vender o para obtener potenciales clientes.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Hero ====
        ======================================= -->

                <!-- =====================================
        ==== Start Numbers -->

        <div class="numbers section-padding bg-img bg-fixed" data-scroll-index="2" data-overlay-dark="8" data-background="img/agency.jpg">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-heart"></span>
                            <h3 class="count">31</h3>
                            <h6>Clientes Contentos</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-case"></span>
                            <h3 class="count">14</h3>
                            <h6>Proyectos Completados</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-sm50">
                            <span class="icon icon-basic-download"></span>
                            <h3 class="count">23</h3>
                            <h6>Campañas Sociales</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center">
                            <span class="icon icon-basic-book-pencil"></span>
                            <h3 class="count">1954</h3>
                            <h6>Días Trabajando</h6>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Numbers ====
        ======================================= -->

        <!-- =====================================
        ==== Start Section-box -->

        <section class="section-box">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-6 half-content bg-gray">
                        <div class="box-white">

                            <div class="content mb-50">
                                <h3 class="mb-15">Por qué elegirnos?</h3>
                                <p>Si tienes un negocio online o un proyecto para hacer realidad, puedes contar con nuestra experiencia y conocimiento para poder emprender tu sueño, ya sea a través de una tienda online o una web institucional. Nosotros nos ocupamos de poner todos los medios posibles para que tu negocio sea viable en internet, desde el diseño web, el posicionamiento web (SEO y SEM), marketing online y gestión de redes sociales.</p>
                            </div>

                            <!-- accordion -->

                            <div class="accordion">

                                <div class="item active">
                                    <div class="title">
                                        <h6>Campañas Publicitarias</h6>
                                    </div>
                                    <div class="accordion-info active">
                                        <p>Captamos tus necesidades, escuchamos tus ideas y las potenciamos. Identificamos tu competencia para diferenciarnos de ella. Hacemos campañas en Redes Sociales y Google.</p>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="title">
                                        <h6>Community Management</h6>
                                    </div>
                                    <div class="accordion-info">
                                        <p>Formamos parte del día a día de tus clientes en las Redes Sociales y potenciamos tu marca planificando contenidos con gran contacto visual.</p>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="title">
                                        <h6>Diseños Web y Software</h6>
                                    </div>
                                    <div class="accordion-info">
                                        <p>Hacemos páginas web con un diseño único y profesional. Ya sean informativas o e-commerce. También ofrecemos software a medida.</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 bg-img half-img bgimg-height valign" data-background="img/background-books-bookshelves-1193603.jpg">
                        <a class="vid text-center full-width" href="https://vimeo.com/304601834">
                            <span class="vid-butn">
                                <i class="fas fa-play-circle"></i>
                            </span>
                        </a>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Section-box ====
        ======================================= -->

        <!-- =====================================
        ==== Start Clients -->

        <div class="clients text-center pt-50 pb-50">
            <div class="container">
                <div class="row">
                    
                    <div class="owl-carousel owl-theme col-sm-12">
                        <a href="#0">
                            <img src="img/clients-logo/alfajores.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/boardingpet.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/boutique.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/bvm.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/cinco soles.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/customwood.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/estilo.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/hierro.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/nest.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/quina.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/restauracion.jpg" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Clients ====
        ======================================= -->


        <!-- =====================================
        ==== Start Contact-Info -->

        <div class="contact-info section-box" data-scroll-index="6">
            <div class="container-fluid">
                <div class="row">
                    
                    <!-- Contact Info -->

                    <div class="info bg-img col-md-6" data-overlay-dark="8" data-background="img/cable-call-communication-33999.jpg">

                        <div class="gradient">

                            <div class="item">
                                <span class="icon icon-basic-smartphone"></span>
                                <div class="cont">
                                    <h6>Teléfono : </h6>
                                    <p>+54 9 11 6543-5170</p>
                                </div>
                            </div>

                            <div class="item">
                                <span class="icon icon-basic-geolocalize-05"></span>
                                <div class="cont">
                                    <h6>Dirección : </h6>
                                    <p>Universidad de Buenos Aires 2165</p>
                                    <p>Barrio Cuba, Villa de Mayo</p>
                                </div>
                            </div>

                            <div class="item">
                                <span class="icon icon-basic-mail"></span>
                                <div class="cont">
                                    <h6>Email : </h6>
                                    <p>hola@wemanagement.com.ar</p>
                                </div>
                            </div>

                            <div class="social">
                                <a href="https://www.facebook.com/wemanagement.ok/" class="icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <!-- <a href="#0" class="icon">
                                    <i class="fab fa-twitter"></i>
                                </a> -->
                                <!-- <a href="#0" class="icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a> -->
                                <a href="https://www.instagram.com/wemanagement.ok/" class="icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/53425912" class="icon">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- The Map -->

                    <div class="map col-md-6 bgimg-height-sm">

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3288.140251460249!2d-58.69440028477312!3d-34.49932848048665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbd9390304585%3A0x67498ade7c495722!2sWe%20Management!5e0!3m2!1ses-419!2sar!4v1595539723879!5m2!1ses-419!2sar" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Contact-Info ====
        ======================================= -->


        <!-- =====================================
        ==== Start Contact -->

        <section class="contact section-padding" id="contacto" data-scroll-index="6">
            <div class="container">
                <div class="row">
                    
                    <div class="section-head text-center col-sm-12">
                        <h4 style="margin-top: 15px">Contacto</h4>
                        <h6>Hace tu Consulta</h6>
                    </div>

                    <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10">
                        <form name="contact-form" class="form" id="contact-form" method="post" action="contact.php">
                            

                            <input type="hidden" name="utm_source" value="<?= (isset($_GET['utm_source'])) ? $_GET['utm_source'] : '' ?>">
                            <input type="hidden" name="utm_term" value="<?= (isset($_GET['utm_term'])) ? $_GET['utm_term'] : '' ?>">
                            <input type="hidden" name="utm_campaign" value="<?= (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign'] : '' ?>">
                            <input type="hidden" name="pais" value="<?= (isset($_GET['pais'])) ? $_GET['pais'] : '' ?>">
                            <input type="hidden" name="utm_medium" value="<?= (isset($_GET['utm_medium'])) ? $_GET['utm_medium'] : '' ?>">
                            <input type="hidden" name="utm_content" value="<?= (isset($_GET['utm_content'])) ? $_GET['utm_content'] : '' ?>">

                            <div class="messages"></div>

                            <div class="controls">

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="organization" type="text" name="organization" placeholder="Empresa" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="form_subject" type="phone" name="phone" placeholder="Teléfono" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="message" placeholder="Mensaje" rows="4" required="required"></textarea>
                                        </div>
                                    </div>
                                    <div class="g-recaptcha m-auto" data-sitekey="6LfMnL0UAAAAAK7MiHA4AH9614Hw7nPtywgRFZsr"></div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div id="g-recaptcha-error" class="mb-2"></div>
                                        <button id="botonEnviar" type="submit"><span>Enviar</span></button>
                                    </div>

                                </div>                             
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Contact ====
        ======================================= -->

        <?php include ('partials/footer.php'); ?>

        <?php include ('partials/scripts.php'); ?>

        <div class="Right-zap"></div><!--Right-->
    
        <a href="https://api.whatsapp.com/send?text=Hola%20quisiera%20m%C3%A1s%20informaci%C3%B3n&phone=%2B5491154967570" class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
        </a>
    </body>
</html>
<!DOCTYPE html>
 <html lang="es">

    <?php include ('partials/head.php'); ?>

    <body>

     <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFVDGL6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    	<!-- =====================================
    	==== Start Loading -->

    	<div class="loading">
    		<div class="text-center middle">
    			<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
    		</div>
    	</div>
        
    	<!-- End Loading ====
    	======================================= -->

        <?php include ('partials/nav.php'); ?>

        <?php include ('partials/header.php'); ?>

        <!-- =====================================
        ==== Start Hero -->

        <section class="hero section-padding" data-scroll-index="1">
            <div class="container">
                <div class="row">
                    
                    <div class="intro offset-lg-1 col-lg-10 text-center mb-80">
                        <h3>BIENVENIDO</h3>
                        <h4>Quienés Somos y Que Hacemos?</h4>
                        <p>Somos un equipo de profesionales creativos que comprende cómo desarrollar y utilizar el contenido como un medio para aumentar tus ingresos y construir relaciones significativas. Creamos contenido de valor y relevante orientado a generar engagement, loyalty y conversión. Desarrollamos e implementamos un plan táctico de Content Marketing para que la marca pueda construir lazos fuertes a largo plazo con los consumidores de mayor valor.</p>
                        <br>
                        <p>¿Sabías que tener un sitio web sin tráfico es como tener un local cerrado? Posicionamos tu negocio con SEO y SEM, pagás solo por clics, si no te visitan no pagás. Conseguimos tu cliente adecuado. También en las redes el contenido orgánico murió. Nosotros Realizamos campañas de Facebook e Instagram con segmentación inteligente, vamos más alla de la inversión habitual de los posteos, nosotros buscamos clientes potenciales generando interacciones y leads.</p>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-book-pen"></span>
                            <h5>Diseño Creativo</h5>
                            <p>Diseñamos contenido digital para tu empresa, ya sean imagenes para subir en las redes, diseños web, manual de marca, imagenes para anuncios, etc...</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-cards-diamonds"></span>
                            <h5>Community Management</h5>
                            <p>El community management es sin lugar a dudas la mejor manera de encontrar buenos clientes. Somos responsables de la gestión y el desarrollo de la comunidad online de su marca o empresa en el mundo digital, manteniendo relaciones estables y dudaderas con sus clientes.</p>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="item text-center">
                            <span class="icon icon-basic-pencil-ruler"></span>
                            <h5>Campañas Publicitarias</h5>
                            <p>Posicionamos tu Marca en Internet, hacemos campañas publicitarias en Google y Redes Sociales. Todo tipo de campañas ya sea para vender o para obtener potenciales clientes.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Hero ====
        ======================================= -->


        <!-- =====================================
        ==== Start Section-box -->

        <section class="section-box" data-scroll-index="1">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-lg-6 bg-img half-img bgimg-height" data-background="img/business-computer-device-35550.jpg"></div>

                    <div class="col-lg-6 half-content bg-gray">
                        <div class="box-white">

                            <div class="content mb-50">
                                <h5 class="sm-line-height">Marketing is not about the things you do, is about the stories you tell.</h5>
                                <p class="mb-15">Seth Godin</p>
                                <p>Hace años que ayudamos a Pymes a generar nuevas oportunidades de negocio utilizando las redes como nuestro gran pilar, posicionándolo en la web con estretategias y contenido en el universo online, con el objetivo de alcanzar potenciales clientes  mediante un dialogo transparente, ganando credibilidad y confianza, asi como lograr un feedback totalmente personalizado.</p>
                            </div>

                            <!-- Skills -->

                            <div class="skills">
                                <div class="skill-item">
                                    <h6>Diseño Creativo</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="90%"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <h6>Community Management</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="85%"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <h6>Campañas Publicitarias</h6>
                                    <div class="skill-progress">
                                        <div class="progres" data-value="99%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Section-box ====
        ======================================= -->


        <!-- =====================================
        ==== Start Services -->

        <section class="services section-padding" data-scroll-index="2">
            <div class="container">
                <div class="row">

                    <div class="section-head text-center col-sm-12">
                        <h4>Nuestros Servicios</h4>
                        <h6>We Management</h6>
                    </div>
                    
                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-book-pencil"></span>
                            <h6>Diseño Web</h6>
                            <p>Hacemos páginas web, ya sea informativa o un sistema a medida. Optimizadas para todos los dispositivos</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-pencil-ruler"></span>
                            <h6>Campañas Publicitarias</h6>
                            <p>Posicinamos tu Marca en Internet, hacemos campañas de publicidad en Redes Sociales y Google.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-anchor"></span>
                            <h6>Community Management</h6>
                            <p>Somos responsables de la gestión y el desarrollo de la comunidad online de su marca o empresa en el mundo digital, manteniendo relaciones estables y dudaderas con sus clientes.</p>
                        </div>
                    </div>

                    <hr>

                    <div class="col-lg-4 col-md-6">
                        <div class="item mb-md50">
                            <span class="icon icon-basic-picture"></span>
                            <h6>Diseño Creativo</h6>
                            <p>Diseñamos contenido digital para tu empresa, ya sean imagenes para subir en las redes, diseños web, manual de marca, imagenes para anuncios, etc...</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 bord">
                        <div class="item mb-sm50">
                            <span class="icon icon-basic-ipod"></span>
                            <h6>E-Commerce</h6>
                            <p>Tiendas online con un diseño profesional, a medida y optimizado para todos los dispositivos. Te dejamos la web lista para que la puedas gestionar en un back-office amigable. Trabajamos con Mercado Shops, Tienda Nube, Shopify o podemos armarte una tienda a medida.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="item">
                            <span class="icon icon-basic-gear"></span>
                            <h6>Soporte Personalizado</h6>
                            <p>Vas a poder contactarte con cualquiera de nuestro equipo para que te responda las preguntas que tengas, en cualquier momento. Te enviamos un Informe Mensual con todas las metricas de tu empresa.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Services ====
        ======================================= -->


        <!-- =====================================
        ==== Start Numbers -->

        <div class="numbers section-padding bg-img bg-fixed" data-scroll-index="2" data-overlay-dark="8" data-background="img/agency.jpg">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-heart"></span>
                            <h3 class="count">31</h3>
                            <h6>Clientes Contentos</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-md50">
                            <span class="icon icon-basic-case"></span>
                            <h3 class="count">14</h3>
                            <h6>Proyectos Completados</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center mb-sm50">
                            <span class="icon icon-basic-download"></span>
                            <h3 class="count">23</h3>
                            <h6>Campañas Sociales</h6>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="item text-center">
                            <span class="icon icon-basic-book-pencil"></span>
                            <h3 class="count">1954</h3>
                            <h6>Días Trabajando</h6>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Numbers ====
        ======================================= -->


        <!-- =====================================
        ==== Start Works -->

        <section class="works section-padding" data-scroll-index="3">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="section-head text-center col-sm-12">
                        <h4>Nuestro Portfolio</h4>
                        <h6>Últimos Proyectos</h6>
                    </div>

                    <!-- filter links -->
                    <!-- <div class="filtering text-center mb-30 col-sm-12">
                        <div class="filter">
                            <span data-filter='*' class="active">All</span>
                            <span data-filter='.brand'>Campañas</span>
                            <span data-filter='.web'>Web</span>
                            <span data-filter='.graphic'>Diseños</span>
                        </div>
                    </div> -->

                    <div class="clearfix"></div>

                    <!-- gallery -->
                    <div class="text-center full-width d-flex flex-wrap">

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="img/portfolio/contagram.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Landing Pages y Campañas</p>
                                        <h6>Contagram</h6>
                                        <!-- <a href="img/portfolio/cliente-estilo.jpg" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a> -->
                                        <a href="https://contagram.com/">
                                            <span class="icon icon-basic-link"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <!-- gallery item -->
                         <div class="col-lg-3 col-md-6 items graphic">
                            <div class="item-img">
                                <img src="img/portfolio/focet.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Web, Diseños & Campañas</p>
                                        <h6>Griferías Focet</h6>
                                        <!-- <a href="img/portfolio/cliente-quina.jpg" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a> -->
                                        <a href="https://focetgriferia.com.ar">
                                            <span class="icon icon-basic-link"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items web">
                            <div class="item-img">
                                <img src="img/portfolio/pet.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Web, Diseño & Campañas</p>
                                        <h6>Boarding Pet</h6>
                                        <!-- <a href="img/portfolio/cliente-boarding.jpg" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a> -->
                                        <a href="https://www.boardingpet.com.ar/">
                                            <span class="icon icon-basic-link"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="img/portfolio/restauracion.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Web, Diseño & Campañas</p>
                                        <h6>Restauración Central</h6>
                                        <!-- <a href="img/portfolio/cliente-rest.jpg" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a> -->
                                        <a href="https://www.restauracioncentral.com/">
                                            <span class="icon icon-basic-link"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items graphic">
                            <div class="item-img">
                                <img src="img/portfolio/volkswagen.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Diseño & Campañas</p>
                                        <h6>Volkswagen Argentina</h6>
                                        <!-- <a href="img/portfolio/cliente-hierro.jpg" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a> -->
                                        <a href="https://www.facebook.com/VolkswagenZonaCentro/">
                                            <span class="icon icon-basic-link"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items web">
                            <div class="item-img">
                                <img src="img/portfolio/montaggio.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Landing, Diseño & Campañas</p>
                                        <h6>Montaggio Arquitectura</h6>
                                        <a href="https://contacto.montaggioarq.com.ar" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="img/portfolio/clever.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Web, Diseño & Campañas</p>
                                        <h6>Clever Broker</h6>
                                        <a href="https://cleverbroker.com.ar/" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- gallery item -->
                        <div class="col-lg-3 col-md-6 items brand">
                            <div class="item-img">
                                <img src="img/portfolio/tejerina.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <p>Landing, Diseños & Campañas</p>
                                        <h6>Expreso Tejerina</h6>
                                        <a href="https://www.expresotejerina.com/" class="popimg">
                                            <span class="icon icon-basic-eye"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>

        <!-- End Works ====
        ======================================= -->


        <!-- =====================================
        ==== Start Section-box -->

        <section class="section-box">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-lg-6 half-content bg-gray">
                        <div class="box-white">

                            <div class="content mb-50">
                                <h3 class="mb-15">Por qué elegirnos?</h3>
                                <p>Si tienes un negocio online o un proyecto para hacer realidad, puedes contar con nuestra experiencia y conocimiento para poder emprender tu sueño, ya sea a través de una tienda online o una web institucional. Nosotros nos ocupamos de poner todos los medios posibles para que tu negocio sea viable en internet, desde el diseño web, el posicionamiento web (SEO y SEM), marketing online y gestión de redes sociales.</p>
                            </div>

                            <!-- accordion -->

                            <div class="accordion">

                                <div class="item active">
                                    <div class="title">
                                        <h6>Campañas Publicitarias</h6>
                                    </div>
                                    <div class="accordion-info active">
                                        <p>Captamos tus necesidades escuchamos tus ideas y las potenciamos. Identificamos tu competencia para diferenciarnos de ella. Hacemos campañas en Redes Sociales y Google.</p>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="title">
                                        <h6>Community Management</h6>
                                    </div>
                                    <div class="accordion-info">
                                        <p>Formamos parte del día a día de tus clientes en las Redes Sociales y potenciamos tu marca planificando contenidos con gran contacto visual.</p>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="title">
                                        <h6>Diseños Web y Software</h6>
                                    </div>
                                    <div class="accordion-info">
                                        <p>Hacemos páginas web con un diseño único y profesional. Ya sean informativas o e-commerce. También ofrecemos software a medida.</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 bg-img half-img bgimg-height valign" data-background="img/background-books-bookshelves-1193603.jpg">
                        <a class="vid text-center full-width" href="https://vimeo.com/304601834">
                            <span class="vid-butn">
                                <i class="fas fa-play-circle"></i>
                            </span>
                        </a>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Section-box ====
        ======================================= -->
            
        <!-- =====================================
        ==== Start Testimonials --> 

        <section class="testimonials carousel-single section-padding bg-img bg-fixed" data-overlay-dark="7" data-background="img/background-books-bookshelves-1193603.jpg">
            <div class="container">
                <div class="row">
                    
                    <div class="offset-lg-1 col-lg-10">

                        <div class="owl-carousel owl-theme text-center">
                            <div class="item">
                                <div class="client-img">
                                    <img src="img/clients/fatima.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h6>Fatima Estevez <span>Estilo Sillas</span></h6>
                                </div>
                                <p>Solo vendia por Instagram y cuando contrate We me cambiaron la cara de la empresa y con campañas me ayudaron a vender en diferentes redes. Es increible lo que puede crecer una empresa si invierte en publicidad.</p>
                            </div>
                            <div class="item">
                                <div class="client-img">
                                    <img src="img/clients/jx.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h6>Juan Cruz Albani <span>Contagram</span></h6>
                                </div>
                                <p>Tenemos un sistema de gestión online. Los chicos de We hacen un muy buen trabajo con las campañas online. Obtenemos alrededor de 1000 registros por mes.</p>
                            </div>
                            <div class="item">
                                <div class="client-img">
                                    <img src="img/clients/gonchi.jpg" alt="">
                                </div>
                                <div class="info">
                                    <h6>Gonzalo Migliore <span>Hierro Demolición</span></h6>
                                </div>
                                <p>Vendemos muebles a medida y no tenía ni página web, me ayudaron mucho a mejorar mi empresa y su presencia online.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Testimonials ====
        ======================================= -->

        <!-- =====================================
        ==== Start Subscribe -->

        <div class="subscribe section-padding bg-gray bg-img" data-scroll-index="5" data-background="img/pattern.png">
            <div class="container">
                <div class="row">
                    
                    <div class="offset-lg-3 col-lg-6 offset-md-2 col-md-8 text-center">
                        <h4>Unite a Nuestro Newsletter</h4>
                        <form name="suscribe" id="suscribe" method="POST" action="contact.php">
                            <input type="email" name="suscribe-email" placeholder="Tu E-mail">
                            <button type="submit" class="butn butn-bg"><span>Suscribirme</span></button>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Subscribe ====
        ======================================= -->

        <!-- =====================================
        ==== Start Clients -->

        <div class="clients text-center pt-50 pb-50">
            <div class="container">
                <div class="row">
                    
                    <div class="owl-carousel owl-theme col-sm-12">
                        <a href="#0">
                            <img src="img/clients-logo/alfajores.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/boardingpet.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/boutique.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/bvm.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/cinco soles.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/customwood.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/estilo.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/hierro.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/nest.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/quina.jpg" alt="">
                        </a>
                        <a href="#0">
                            <img src="img/clients-logo/restauracion.jpg" alt="">
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Clients ====
        ======================================= -->


        <!-- =====================================
        ==== Start Contact-Info -->

        <div class="contact-info section-box" data-scroll-index="6">
            <div class="container-fluid">
                <div class="row">
                    
                    <!-- Contact Info -->

                    <div class="info bg-img col-md-6" data-overlay-dark="8" data-background="img/cable-call-communication-33999.jpg">

                        <div class="gradient">

                            <div class="item">
                                <span class="icon icon-basic-smartphone"></span>
                                <div class="cont">
                                    <h6>Teléfono : </h6>
                                    <p>+54 9 11 6543-5170</p>
                                </div>
                            </div>

                            <div class="item">
                                <span class="icon icon-basic-geolocalize-05"></span>
                                <div class="cont">
                                    <h6>Ddirección : </h6>
                                    <p>Universidad de Buenos Aires 2165</p>
                                    <p>Barrio Cuba, Villa de Mayo</p>
                                </div>
                            </div>

                            <div class="item">
                                <span class="icon icon-basic-mail"></span>
                                <div class="cont">
                                    <h6>Email : </h6>
                                    <p>hola@wemanagement.com.ar</p>
                                </div>
                            </div>

                            <div class="social">
                                <a href="https://www.facebook.com/wemanagement.ok/" class="icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <!-- <a href="#0" class="icon">
                                    <i class="fab fa-twitter"></i>
                                </a> -->
                                <!-- <a href="#0" class="icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a> -->
                                <!-- <a href="#0" class="icon">
                                    <i class="fab fa-behance"></i>
                                </a> -->
                                <a href="https://www.instagram.com/wemanagement.ok/" class="icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <!-- The Map -->

                    <div class="map col-md-6 bgimg-height-sm">
                            <iframe style="width:100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d647.6731884478131!2d-58.68703824187639!3d-34.50995895500772!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbcd6a894c6c5%3A0x7e5614baef7374a0!2sUniversidad+de+Buenos+Aires+2470%2C+B1614BXJ+Villa+de+Mayo%2C+Buenos+Aires!5e0!3m2!1ses!2sar!4v1550271609487" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                </div>
            </div>
        </div>

        <!-- End Contact-Info ====
        ======================================= -->


        <!-- =====================================
        ==== Start Contact -->

        <section class="contact section-padding" id="contacto" data-scroll-index="6">
            <div class="container">
                <div class="row">
                    
                    <div class="section-head text-center col-sm-12">
                        <h4 style="margin-top: 15px">Contacto</h4>
                        <h6>Hace tu Consulta</h6>
                    </div>

                    <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10">
                        <form name="contact-form" class="form" id="contact-form" method="post" action="contact.php">
                            

                            <input type="hidden" name="utm_source" value="<?= (isset($_GET['utm_source'])) ? $_GET['utm_source'] : '' ?>">
                            <input type="hidden" name="utm_term" value="<?= (isset($_GET['utm_term'])) ? $_GET['utm_term'] : '' ?>">
                            <input type="hidden" name="utm_campaign" value="<?= (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign'] : '' ?>">

                            <div class="messages"></div>

                            <div class="controls">

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="organization" type="text" name="organization" placeholder="Empresa" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input id="form_subject" type="phone" name="phone" placeholder="Teléfono" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="form_message" name="message" placeholder="Mensaje" rows="4" required="required"></textarea>
                                        </div>
                                    </div>
                                    <div class="g-recaptcha m-auto" data-sitekey="6LfMnL0UAAAAAK7MiHA4AH9614Hw7nPtywgRFZsr"></div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div id="g-recaptcha-error" class="mb-2"></div>
                                        <button id="botonEnviar" type="submit"><span>Enviar</span></button>
                                    </div>

                                </div>                             
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </section>

        <!-- End Contact ====
        ======================================= -->

        <?php include ('partials/footer.php'); ?>

        <?php include ('partials/scripts.php'); ?>


    
    </body>
</html>